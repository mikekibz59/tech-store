class ProductsController < ApplicationController
    
    def index
        @products = Product.all.order('created_at DESC')
        
    end
    
    def add_to_cart
        #if there is no cart create one
        if session[:cart_id].blank?
            cart= Cart.create(status: 'pending')
            session[:cart_id]= cart.id
        else
            #if the cart is present find it
            cart=Cart.find(session[:cart_id])
        end
        #find the specific product per say and equate to the cart 
    product= Product.find(params[:id])
    #then fill the cart using cartship values which inherits it from the products and increments the quantity atrribute in the model cartship
    cart.cartships.create(product_id: product.id , quantity: 1)
    redirect_to cart
    end
    
end
