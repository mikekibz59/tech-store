class Product < ActiveRecord::Base
   validates :title, :presence => true
   validates :price, :presence => true
   has_many :cartships
  # belongs_to :cart, through: :cartships
   
    mount_uploader :image, ImageUploader
end
