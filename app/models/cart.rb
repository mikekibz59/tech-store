class Cart < ActiveRecord::Base
    
    has_many :cartships
    has_many :products, through: :cartships
    
    #for testing purposes
    def total_price
        cartships.map{|cartship| cartship.product.price * cartship.quantity}.sum
    end
end
